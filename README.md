# Backpack Dynamic Sidebar



## Getting started

This package provides

    - migrations to handle the dynamic sidebar
    - crud controller to administrate the sidebar
    - sidebar view 
    - command to generate crud controller without affecting the sidebar

Routes are automatically registered in the provider

## install 

    sail artisan kda:sidebars:install 

### or manually

    sail artisan vendor:publish --provider="KDA\Backpack\DynamicSidebar\ServiceProvider" --tag="migrations" --force
    sail artisan vendor:publish --provider="KDA\Backpack\DynamicSidebar\ServiceProvider" --tag="views" --force

## @fdt2k/backpack-devtools
    seeds are automatically handled by devtools