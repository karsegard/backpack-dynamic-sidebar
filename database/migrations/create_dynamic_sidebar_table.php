<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateDynamicSidebarTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::disableForeignKeyConstraints();

        Schema::create('sidebars', function (Blueprint $table) {
            $table->id();
            $table->string('label');
            $table->string('icon')->nullable()->default('la-question');

            $table->string('route')->nullable();

            $table->string('role')->nullable();
            $table->string('permission')->nullable();
            $table->boolean('from_children')->default(0);

            $table->enum('behavior',['hide','disable'])->default('disable');
            $table->unsignedInteger('parent_id')->nullable();
            $table->unsignedInteger('lft')->nullable()->default(0);
            $table->unsignedInteger('rgt')->nullable()->default(0);
            $table->unsignedInteger('depth')->nullable()->default(0);

            $table->text('display_location')->nullable();
            $table->timestamps();
        });


        Schema::create('permission_sidebar', function (Blueprint $table) {
            $table->unsignedBigInteger('sidebar_id');
            $table->unsignedBigInteger('permission_id');
            $table->foreign('permission_id')
                ->references('id')
                ->on('permissions')
                ->onDelete('cascade'); 
            $table->foreign('sidebar_id')
            ->references('id')
            ->on('sidebars')
            ->onDelete('cascade'); 
            $table->primary(['sidebar_id', 'permission_id'], 'sidebar_id_permission_id_primary');
        });

        Schema::create('role_sidebar', function (Blueprint $table) {
            $table->unsignedBigInteger('sidebar_id');
            $table->unsignedBigInteger('role_id');
            $table->foreign('role_id')
            ->references('id')
            ->on('roles')
            ->onDelete('cascade'); 
            $table->foreign('sidebar_id')
            ->references('id')
            ->on('sidebars')
            ->onDelete('cascade'); 
            $table->primary(['sidebar_id', 'role_id'], 'sidebar_id_roles_id_primary');
        });




        Schema::enableForeignKeyConstraints();
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sidebars');
    }
}
