@foreach ($sidebar as $item)
    @if ($item->is_visible)
        @if ($item->children->count() > 0)
            @if ($item->has_rights)
                <li class="nav-item nav-dropdown ">
            @endif
            <a class="nav-link {{ $item->has_rights === false ? 'disabled' : 'nav-dropdown-toggle' }} " href="#"><i
                    class="nav-icon la  {!! $item->icon !!}"></i>
                {!! $item->label !!}</a>
            <ul class="nav-dropdown-items">
                @foreach ($item->children as $child)
                    @if($child->behavior=='disabled'|| $child->has_rights )
                    <li class="nav-item"><a class="nav-link {{ $child->has_rights === false ? 'disabled' : '' }} "
                            href="{{ backpack_url($child->route) }}"><i
                                class="la  {!! $child->icon !!} nav-icon"></i>
                            {!! $child->label !!}</a></li>
                    @endif
                @endforeach
            </ul>
            @if ($item->has_rights)
                </li>
            @endif
        @else
            <li class="nav-item"><a class="nav-link {{ $item->has_rights === false ? 'disabled' : '' }} "
                    href="{{ backpack_url($item->route) }}"><i class="la {!! $item->icon !!} nav-icon"></i>
                    {!! $item->label !!}</a></li>
        @endif
    @endif

@endforeach
