<!-- This file is used to store topbar (left) items -->

{{-- <li class="nav-item px-3"><a class="nav-link" href="#">Dashboard</a></li>
<li class="nav-item px-3"><a class="nav-link" href="#">Users</a></li>
<li class="nav-item px-3"><a class="nav-link" href="#">Settings</a></li> --}}

<style>
    .app-header .nav-item .nav-link .badge.kda-sidebar {
        left: 0%;
    }

</style>
@foreach ($sidebar as $item)

    @php
        $location = $item->display_location;
        $notification = 0;
        $should_notify = false;
        if ($location['notify'] == 1) {
            $should_notify = true;
            try {
                $model = $location['notify_method'];
                if ($model) {
                    try{
                    $notification = call_user_func($model);
                    }catch (\Exception $e){
                        $notification = '???';
                    }
                }
            } catch (TypeError $e) {
                $should_notify = false;
                dump($e);
            }
        }
        
        $parameters = [];
        
        if ($item->display_location['notify_route_parameters']) {
            $parameters = json_decode($item->display_location['notify_route_parameters'], true);
        }
        
        $parameters = collect($parameters)
            ->map(function ($item) {
                return $item['key'] . '=' . $item['value'];
            })
            ->join('&');
        
    @endphp

    @if ($item->is_visible)
        @if ($item->children->count() > 0)
            @if ($item->has_rights)
                <li class="nav-item px-3 nav-dropdown ">
            @endif
            <a class="nav-link {{ $item->has_rights === false ? 'disabled' : 'nav-dropdown-toggle' }} " href="#">
                {!! $item->label !!}</a>
            <ul class="nav-dropdown-items">
                @foreach ($item->children as $child)
                    <li class="nav-item"><a class="nav-link {{ $child->has_rights === false ? 'disabled' : '' }} "
                            href="{{ backpack_url($child->route) }}"><i
                                class="la  {!! $child->icon !!} nav-icon"></i>
                            {!! $child->label !!}</a></li>
                @endforeach
            </ul>
            @if ($item->has_rights)
                </li>
            @endif
        @else
            <li class="nav-item px-4"><a class="nav-link {{ $item->has_rights === false ? 'disabled' : '' }} "
                    href="{{ backpack_url($item->route) . '?' . $parameters }}">
                    {!! $item->label !!}
                    @if ($should_notify)
                        <span
                            class="badge badge-pill badge-{{ $notification > 0 ? 'danger' : 'primary' }} kda-sidebar">{{ $notification }}</span>
                </a>
        @endif
        </li>
    @endif
@endif

@endforeach
