<?php

namespace KDA\Backpack\DynamicSidebar\Commands;

use GKA\Noctis\Providers\AuthProvider;
use Illuminate\Console\Command;
use Illuminate\Support\Str;
use Config;

class Install extends Command
{
   
    /**
     * The console command name.
     *
     * @var string
     */
    protected $signature = 'kda:sidebars:install';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = '';


    /**
     * Execute the console command.
     *
     * @return void
     */
    public function handle()
    {
        $this->call('vendor:publish', ['--provider' => "KDA\Backpack\DynamicSidebar\ServiceProvider",'--tag'=>'migrations']);
        $this->call('vendor:publish', ['--provider' => "KDA\Backpack\DynamicSidebar\ServiceProvider",'--tag'=>'config']);
        $this->call('vendor:publish', ['--provider' => "KDA\Backpack\DynamicSidebar\ServiceProvider",'--tag'=>'views']);
        $this->call('migrate');

        $this->call('db:seed', ['--class' => "\KDA\Backpack\DynamicSidebar\Seeders\InstallSeeder"]);



    }
}
