<?php

namespace KDA\Backpack\DynamicSidebar;


class DevTools
{

    public static function seeds(): array
    {
        return [
            'sidebars',
            'permission_sidebar',
            'role_sidebar',
        ];
    }

    public static function sidebars(): array
    {

        return [
            [
                'label' => 'Sidebars',
                'icon' => 'la-route',
                'route' => 'sidebar',
                'from_children' => '0',
                'behavior' => 'disable',
                'lft' => '52',
                'rgt' => '53',
                'depth' => '1',
                'children' => []
                
            ],

        ];
    }
    
    
}
