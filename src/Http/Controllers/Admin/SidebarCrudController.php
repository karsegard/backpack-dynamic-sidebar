<?php

namespace KDA\Backpack\DynamicSidebar\Http\Controllers\Admin;

use KDA\Backpack\DynamicSidebar\Http\Requests\SidebarRequest;
use Backpack\CRUD\app\Http\Controllers\CrudController;
use Backpack\CRUD\app\Library\CrudPanel\CrudPanelFacade as CRUD;

/**
 * Class NavigationCrudController
 * @package App\Http\Controllers\Admin
 * @property-read \Backpack\CRUD\app\Library\CrudPanel\CrudPanel $crud
 */
class SidebarCrudController extends CrudController
{
    use \Backpack\CRUD\app\Http\Controllers\Operations\ListOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\CreateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\UpdateOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\DeleteOperation;
    use \Backpack\CRUD\app\Http\Controllers\Operations\ReorderOperation;
    use \KDA\Backpack\Auth\Http\Controllers\Traits\CrudPermission;

    /**
     * Configure the CrudPanel object. Apply settings to all operations.
     *
     * @return void
     */
    public function setup()
    {
        CRUD::setModel(\KDA\Backpack\DynamicSidebar\Models\Sidebar::class);
        CRUD::setRoute(config('backpack.base.route_prefix') . '/sidebar');
        CRUD::setEntityNameStrings('sidebar', 'sidebars');
        $this->loadPermissions('sidebar');
    }

    protected function setupReorderOperation()
    {
        // define which model attribute will be shown on draggable elements
        $this->crud->set('reorder.label', 'label');
        // define how deep the admin is allowed to nest the items
        // for infinite levels, set it to 0
        $this->crud->set('reorder.max_level', 3);
    }

    /**
     * Define what happens when the List operation is loaded.
     *
     * @see  https://backpackforlaravel.com/docs/crud-operation-list-entries
     * @return void
     */
    protected function setupListOperation()
    {
        kda_no_warranty_alert();
        CRUD::column('label')->label('Nom');
        CRUD::addColumn([
            'type' => 'closure',
            'function' => function ($entry) {
                return '<i class="nav-icon la ' . $entry->icon . '"></i>';
            },
            'wrapper' => [
                'element' => 'span',
                'class' => function ($crud, $column, $entry, $related_key) {
                    return 'nav-icon la la-' . $entry->icon;
                },
            ],
            'name' => 'icon'
        ]);
        CRUD::addColumn([
            'label' => 'Parent',
            'type' => 'select',
            'name' => 'parent_id',
            'entity' => 'parent',
            'attribute' => 'label',
            'model' => "\KDA\Backpack\DynamicSidebar\Models\Sidebar",
        ]);

        CRUD::addColumn(
            [
                // 1-n relationship
                'label' => "Roles", // Table column heading
                'type' => "relationship",
                'orderable'  => true,
                'name' => 'roles', // the column that contains the ID of that connected entity;
                'suffix' => '',
                'orderLogic' => function ($query, $column, $columnDirection) {
                    return
                        $query->leftJoin('role_sidebar', 'sidebar_id', '=', 'sidebars.id')
                        ->leftJoin('roles', 'roles.id', '=', 'role_sidebar.role_id')
                        ->orderBy('roles.name', $columnDirection)->select('sidebars.*');
                }
            ]
        );

        CRUD::addColumn(
            [
                // 1-n relationship
                'label' => "Permissions", // Table column heading
                'type' => "relationship",
                'orderable'  => true,
                'name' => 'permissions', // the column that contains the ID of that connected entity;
                'suffix' => '',
                'orderLogic' => function ($query, $column, $columnDirection) {
                    $query->leftJoin('permission_sidebar', 'sidebar_id', '=', 'sidebars.id')
                        ->leftJoin('permissions', 'permissions.id', '=', 'permission_sidebar.permission_id')
                        ->orderBy('permissions.name', $columnDirection)->select('sidebars.*');
                }
            ]
        );
       
        /**
         * Columns can be defined using the fluent syntax or array syntax:
         * - CRUD::column('price')->type('number');
         * - CRUD::addColumn(['name' => 'price', 'type' => 'number']);
         */
    }

    /**
     * Define what happens when the Create operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-create
     * @return void
     */
    protected function setupCreateOperation()
    {
        CRUD::setValidation(SidebarRequest::class);

        CRUD::field('label')
            ->label('Nom')
            ->wrapper(['class' => 'col-md-12'])
            ->hint('')
            ->tab('Menu');
     
       
        CRUD::field('route')
            ->label('Route')
            ->wrapper(['class' => 'col-md-12'])
            ->hint('')
            ->tab('Route');

        CRUD::addField([

            'name' => 'icon',
            'label' => 'Icon',
            'type' => 'text',
            'iconset' => 'lineawesome',
            'tab' => 'Menu',
            'wrapper' => ['class' => 'col-md-12'],
            'default' => 'la-question'
        ]);

        CRUD::addField([
            'name' => 'behavior',
            'type' => 'enum',
            'tab' => 'Permissions',

        ]);
        CRUD::addField([
            'name' => 'from_children',
            'type' => 'toggle-label',
            'label' => 'Use Childrens permissions',
            'tab' => 'Permissions',

            'view_namespace' => 'kda-backpack-custom-fields::fields',
        ]);

        CRUD::addField([
            'type' => "relationship",
            'name' => 'roles', // the method on your model that defines the relationship
            'label' => 'Roles', // the method on your model that defines the relationship
            'tab' => 'Permissions',

            'wrapper' => ['class' => 'col-md-6'],
        ]);
        CRUD::addField([
            'type' => "relationship",
            'name' => 'permissions', // the method on your model that defines the relationship
            'label' => 'Permissions', // the method on your model that defines the relationship
            'tab' => 'Permissions',

            'wrapper' => ['class' => 'col-md-6'],
        ]);
        CRUD::addField([
            'name'     => 'sidebar', 
            'label'    => "Barre latérale", 
            'default' => true,
            'type'=> 'toggle-label',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'tab'=>'Menu',
            'fake'     => true, 
            'wrapper' => ['class' => 'col-md-4'],

            'store_in' => 'display_location' 
        ]);
        CRUD::addField([
            'name'     => 'top_right', 
            'label'    => "Menu haut droite", 
            'default' => false,
            'type'=> 'toggle-label',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'tab'=>'Menu',
            'fake'     => true, 
            'wrapper' => ['class' => 'col-md-4'],

            'store_in' => 'display_location' 
        ]);
        CRUD::addField([
            'name'     => 'top_left', 
            'label'    => "Menu haut gauche", 
            'default' => false,
            'type'=> 'toggle-label',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'tab'=>'Menu',
            'fake'     => true, 
            'wrapper' => ['class' => 'col-md-4'],

            'store_in' => 'display_location' 
        ]);
        CRUD::addField([
            'name'     => 'notify', 
            'label'    => "Notifications", 
            'default' => false,
            'type'=> 'toggle-label',
            'view_namespace' => 'kda-backpack-custom-fields::fields',
            'tab'=>'Notifications',
            'fake'     => true, 
            'store_in' => 'display_location' 
        ]);
        CRUD::addField([
            'name'     => 'notify_method', 
            'label'    => "Méthode", 
            'default' => false,
            'type'=> 'text',
            'tab'=>'Notifications',
            'fake'     => true, 
            'store_in' => 'display_location' 
        ]);
        CRUD::addField([
            'name'     => 'notify_route_parameters', 
            'label'    => "Additional route parameter", 
            
            'tab'=>'Notifications',
            'fake'     => true, 
            'store_in' => 'display_location' ,
            'type'=>'table',
            'entity_singular' => 'option', // used on the "Add X" button
            'columns'         => [
                'key'  => 'Name',
                'value'  => 'Value',
              
            ],
            'max' => 5, // maximum rows allowed in the table
            'min' => 0, // minimum rows allowed in the table
            'wrapper' => ['class' => 'col-md-4'],
        
        ]);
        /**
         * Fields can be defined using the fluent syntax or array syntax:
         * - CRUD::field('price')->type('number');
         * - CRUD::addField(['name' => 'price', 'type' => 'number']));
         */
    }

    /**
     * Define what happens when the Update operation is loaded.
     *
     * @see https://backpackforlaravel.com/docs/crud-operation-update
     * @return void
     */
    protected function setupUpdateOperation()
    {
        $this->setupCreateOperation();
    }
}
