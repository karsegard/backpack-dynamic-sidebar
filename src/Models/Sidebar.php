<?php

namespace KDA\Backpack\DynamicSidebar\Models;

use Illuminate\Database\Eloquent\Model;

use Backpack\CRUD\app\Models\Traits\CrudTrait;

class Sidebar extends Model
{
    use CrudTrait;
    use Traits\Nested;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'label',
        'icon',
        'route',
        'behavior',
        'lft',
        'rgt',
        'depth',
        'from_children',
        'display_location'
    ];

    protected $casts = [
        'from_children' => 'boolean',
        'display_location'=> 'array'
    ];

    protected $fakeColumns = [
        'display_location'
    ];

    public function roles()
    {
        return $this->belongsToMany(Role::class);
    }

    public function permissions()
    {
        return $this->belongsToMany(Permission::class);
    }
    //   if(auth({$guard})->check() && auth({$guard})->user()->hasRole({$role})): ";




    public function getIsVisibleAttribute()
    {
        if ($this->behavior === 'hide') {
            return $this->has_rights;
        }
        return true;
    }

    public function getHasRightsAttribute()
    {
        $user = auth()->user();

        if ($user->hasRole('admin')) {
            return true;
        }

        if ($this->from_children) {
            $hasAccess =false ;
            foreach($this->children as $child){
                if($child->has_rights){
                    $hasAccess = true;
                    break;
                }

            }

            return $hasAccess;
        } else {

            $roles = $this->roles->pluck('name')->toArray();
            $permissions = $this->permissions->pluck('name')->toArray();


            if (count($roles) == 0 && count($permissions) == 0) {
                return true;
            }



            $has_roles = $user->hasAnyRole($roles);
            $has_permission = $user->hasAnyPermission($permissions);
            //   dump($roles,$this->attributes['label'],$has_roles,$user->can($permissions));

            if ($has_roles || $has_permission) {

                return true;
            }
            return false;
        }
    }

    public function scopeTopLeft($query){
        return $query->whereJsonContains('sidebars.display_location->top_left','1');
    }
}
