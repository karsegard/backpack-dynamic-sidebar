<?php

namespace KDA\Backpack\DynamicSidebar\Seeders;

use Illuminate\Database\Seeder as LaravelSeeder;

class InstallSeeder extends LaravelSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {


      $sidebars =  \KDA\Backpack\DynamicSidebar\DevTools::sidebars() ?? [];

      
      $auth = (class_exists("\\KDA\\Backpack\\Auth\\DevTools")&& method_exists("\\KDA\\Backpack\\Auth\\DevTools","sidebars") ? \KDA\Backpack\Auth\DevTools::sidebars(): []);

      

      $seeds = [
        ...$sidebars,
        ...$auth
      ];

      foreach($seeds as $seed){
        $this->createItem($seed);
      }
    }

    public function createItem($item,$parent= null){
      
      $s = new \KDA\Backpack\DynamicSidebar\Models\Sidebar();
      if($parent!==null){
        $s->parent_id = $parent->id;
      }
      foreach($item as $key => $val){
        if($key !=='children'){
          $s->$key = $val;
        }else {
          $s->save();
          foreach($val as $child){
            $this->createItem($child,$s);
          }
        }
      }
    }
}
