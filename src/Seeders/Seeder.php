<?php

namespace KDA\Backpack\DynamicSidebar\Seeders;

use Illuminate\Database\Seeder as LaravelSeeder;

class Seeder extends LaravelSeeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
      $this->call([
        SidebarSeeder::class,
        PermissionSidebarSeeder::class,
        RoleSidebarSeeder::class,

      ]);
    }
}
