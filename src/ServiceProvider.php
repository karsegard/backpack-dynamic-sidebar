<?php

namespace KDA\Backpack\DynamicSidebar;

use Illuminate\Contracts\Events\Dispatcher;
use Illuminate\Routing\Router;
use Illuminate\Support\Facades\File;

use KDA\Laravel\PackageServiceProvider;
use KDA\Backpack\DynamicSidebar\Models\Sidebar;
use Illuminate\Support\Facades\View;

class ServiceProvider extends PackageServiceProvider
{

    use \KDA\Laravel\Traits\HasViews;
    use \KDA\Laravel\Traits\HasVendorViews;
    use \KDA\Laravel\Traits\HasConfig;
    use \KDA\Laravel\Traits\HasCommands;
    use \KDA\Laravel\Traits\HasMigration;
    use \KDA\Laravel\Traits\HasRoutes;


    protected $configs= [
     'kda/backpack/sidebar.php'  =>'kda.backpack.sidebar'
    ];

    protected $routes = [
        'backpack/dynamicsidebar.php'
    ];

    protected $publishViewsTo = 'vendor/backpack/base';
    
    protected $vendorViewsNamespace = 'kda-sidebar';
    protected $publishVendorViewsTo = 'vendor/kda/backpack/sidebar';


    protected $_commands = [
      
        Commands\CrudBackpackCommand::class,
        Commands\Install::class,
    ];

    protected function packageBaseDir()
    {
        return dirname(__DIR__, 1);
    }


    public function boot(){
        parent::boot();
        View::composer('kda-sidebar::sidebar_render',function($view){
            
            return $view->with([
               'sidebar'=>Sidebar::getTree()
            ]);
        });
        View::composer('kda-sidebar::topbar_left_render',function($view){
            
            return $view->with([
               'sidebar'=>Sidebar::nestedWith('label')->topLeft()->get()
            ]);
        });
    }


   

}
